import { data } from "../../data";
import {
  ADD_TO_CART,
  CHANGE_AMOUNT,
  DEL_ITEM,
  VIEW_DETAIL,
} from "../constant/shoeConstant";
let initialState = {
  dataShoe: data,
  detailShoe: "",
  cart: [],
};

export const shoeProducer = (state = initialState, action) => {
  switch (action.type) {
    case VIEW_DETAIL: {
      return { ...state,detailShoe:action.payload };
    }
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((shoe) => shoe.id === action.payload.id);
      if (index !== -1) {
        cloneCart[index].soLuong += 1;
      } else {
        cloneCart.push(action.payload);
      }
      // state.cart = cloneCart;
      return { ...state, cart:cloneCart };
    }
    case DEL_ITEM: {
      let cartDel = state.cart.filter((shoe) => shoe.id !== action.payload.id);
      return { ...state,cart:cartDel };
    }
    case CHANGE_AMOUNT: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex(
        (shoe) => shoe.id === action.payload.item.id
      );
      cloneCart[index].soLuong = cloneCart[index].soLuong + action.payload.option;
      if (cloneCart[index].soLuong === 0) {
        cloneCart.splice(index, 1);
      }
      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};
