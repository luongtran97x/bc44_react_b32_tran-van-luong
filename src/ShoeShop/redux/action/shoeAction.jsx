import {
  ADD_TO_CART,
  CHANGE_AMOUNT,
  DEL_ITEM,
  VIEW_DETAIL,
} from "../constant/shoeConstant";

// action creator
export let viewDetailAction = (item) => {
  return {
    type: VIEW_DETAIL,
    payload: item,
  };
};
export let handelAddToCartAction = (item) => {
  return {
    type: ADD_TO_CART,
    payload: item,
  };
};

export let handelDeleteAction = (item) => {
  return {
    type: DEL_ITEM,
    payload: item,
  };
};
export let handelChangeAmountAction = (item, option) => {
  return {
    type: CHANGE_AMOUNT,
    payload: { item, option },
  };
};
