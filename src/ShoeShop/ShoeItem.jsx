import React, { Component } from "react";
import { ADD_TO_CART, VIEW_DETAIL } from "./redux/constant/shoeConstant";
import { connect } from "react-redux";
import { handelAddToCartAction, viewDetailAction } from "./redux/action/shoeAction";

class ShoeItem extends Component {
  render() {
    let { item, handelViewDetail, handelAddToCart } = this.props;
    return (
      <div className="col-4 p-4">
        <div className="card text-left h-100">
          <img className="card-img-top" src={item.image} alt={item.alias} />
          <div className="card-body">
            <h4 className="card-title">{item.name}</h4>
            <p className="card-text">{item.price}$</p>
          </div>
          <button
            onClick={() => handelViewDetail(item)}
            className="btn btn-success"
            data-toggle="modal"
            data-target="#modelIdDetail"
          >
            Detail
          </button>
          <button
            className="btn btn-info "
            onClick={() => handelAddToCart(item)}
          >
            Add To Cart
          </button>
        </div>
      </div>
    );
  }
}

let mapDipatchToProps = (dispatch) => {
  return {
    handelViewDetail: (item) => {
      dispatch(viewDetailAction(item))
    },
    handelAddToCart: (item) => {
      let newItem = { ...item, soLuong: 1 };
      dispatch(
        handelAddToCartAction(newItem)
      );
    },
  };
};
export default connect(null, mapDipatchToProps)(ShoeItem);
