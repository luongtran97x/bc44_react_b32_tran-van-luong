import React, { Component } from "react";
import ShoeList from "./ShoeList";
import ShoeDetail from "./ShoeDetail";
import Cart from "./Cart";
import { connect } from "react-redux";

class ShoeShop extends Component {
  countTotal = (params) => {
    return this.props.cart.reduce((total, item) => {
      return (total += item.soLuong);
    }, 0);
  };

  render() {
    return (
      <div>
        <Cart></Cart>
        <h3 className="text-center">Shoes Shop</h3>
        <div className="text-right container">
          <button
            data-toggle="modal"
            data-target="#modelId"
            className="btn btn-info"
          >
            Cart ({this.countTotal()})
          </button>
        </div>
        <ShoeList></ShoeList>
        <ShoeDetail></ShoeDetail>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeProducer.cart,
  };
};
export default connect(mapStateToProps, null)(ShoeShop);
