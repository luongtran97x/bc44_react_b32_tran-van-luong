import React, { Component } from "react";
import { connect } from "react-redux";

class ShoeDetail extends Component {
  render() {
    let { item } = this.props;
    return (
      <div className="container">
        {/* Modal */}
        <div
          className="modal fade"
          id="modelIdDetail"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content"    style={{ width: "700px", minWidth: "700px" }}>
              <div className="modal-header">
                <h5 className="modal-title text-center text-danger">Detail Shoe</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <table class="table">
                  <thead>
                    <tr className="text-center">
                      <th>Name</th>
                      <th>Price</th>
                      <th>Description</th>
                      <th>Image</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr className="text-center">
                      <td scope="row" width={100}>
                        {item.name}
                      </td>
                      <td>{item.price}$</td>
                      <td  width={500}>{item.description} </td>
                      <td><img src={item.image} width={100} /></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    item: state.shoeProducer.detailShoe,
  };
};
export default connect(mapStateToProps, null)(ShoeDetail);
