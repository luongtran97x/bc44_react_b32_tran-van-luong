import React, { Component } from "react";
import ShoeItem from "./ShoeItem";
import { connect } from "react-redux";

 class ShoeList extends Component {
  renderItem = () => {
    let { shoeData, handelViewDetail, handelAddToCart } = this.props;
    return shoeData.map((item) => {
      return (
        <ShoeItem
          handelAddToCart={handelAddToCart}
          handelViewDetail={handelViewDetail}
          item={item}
        ></ShoeItem>
      );
    });
  };

  render() {
    return (
      <div className="container">
        <div className="row">{this.renderItem()}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    shoeData : state.shoeProducer.dataShoe
  }
}

export default connect(mapStateToProps,null)(ShoeList)