import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_AMOUNT, DEL_ITEM } from "./redux/constant/shoeConstant";
import { handelAddToCartAction, handelChangeAmountAction, handelDeleteAction } from "./redux/action/shoeAction";

class Cart extends Component {
  render() {
    let { cartArr, handelDelete, handelChangeAmount } = this.props;
    return (
      <div>
        {/* Modal */}
        <div
          className="modal fade"
          id="modelId"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div
              className="modal-content"
              style={{ width: "1000px", minWidth: "1000px" }}
            >
              <div className="modal-header">
                <h5 className="modal-title ">CART DETAIL</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body text-center">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Tên Sản Phẩm</th>
                      <th>Giá Tiền</th>
                      <th>Số Lượng</th>
                      <th>Thành Tiền</th>
                      <th>Hình Ảnh</th>
                      <th>Thao Tác</th>
                    </tr>
                  </thead>
                  <tbody>
                    {cartArr.map((item) => {
                      return (
                        <tr key={item.id}>
                          <td scope="row">{item.name}</td>
                          <td>{item.price}$</td>
                          <td>
                            <button
                              className="btn btn-warning "
                              onClick={() => handelChangeAmount(item, 1)}
                            >
                              +
                            </button>
                            <strong className="mx-2">{item.soLuong}</strong>
                            <button
                              className="btn btn-warning "
                              onClick={() => handelChangeAmount(item, -1)}
                            >
                              -
                            </button>
                          </td>
                          <td width={100}>{item.soLuong * item.price}$</td>
                          <td>
                            <img
                              src={item.image}
                              width={100}
                              height={100}
                              alt={item.alias}
                            />
                          </td>
                          <td>
                            <button
                              className="btn btn-danger"
                              onClick={() => {
                                handelDelete(item);
                              }}
                            >
                              Xóa
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                  <tfoot className="text-right h4">
                    <td colSpan={5}>Tổng Tiền:</td>
                    <td>
                      {cartArr
                        .reduce((tongTien, item) => {
                          return (tongTien += item.soLuong * item.price);
                        }, 0)
                        .toLocaleString()}
                      $
                    </td>
                  </tfoot>
                </table>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cartArr: state.shoeProducer.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handelDelete: (item) => {
      dispatch(handelDeleteAction(item));
    },
    handelChangeAmount: (item, option) => {
      dispatch(handelChangeAmountAction(item,option));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
